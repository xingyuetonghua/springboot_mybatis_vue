package com.aaa.springbootmybatisvue.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {

    private Integer code = 200;
    private String msg ="操作成功";
    private T t;

    public Result(T t) {
//        this.code=200;
//        this.msg="操作成功";
        this.t = t;
    }

    public static <T> Result success(T t){
        return  new Result(t);
    }

    public static <T> Result fail(T t){
        Result result = new Result(500,"失败",t);
        return  result;
    }
}
