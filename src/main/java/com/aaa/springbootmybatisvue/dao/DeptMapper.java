package com.aaa.springbootmybatisvue.dao;

import com.aaa.springbootmybatisvue.entity.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
