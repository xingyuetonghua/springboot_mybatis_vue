package com.aaa.springbootmybatisvue.service;

import com.aaa.springbootmybatisvue.entity.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
public interface IDeptService extends IService<Dept> {

}
