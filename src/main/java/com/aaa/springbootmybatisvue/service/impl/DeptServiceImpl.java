package com.aaa.springbootmybatisvue.service.impl;

import com.aaa.springbootmybatisvue.entity.Dept;
import com.aaa.springbootmybatisvue.dao.DeptMapper;
import com.aaa.springbootmybatisvue.service.IDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

}
