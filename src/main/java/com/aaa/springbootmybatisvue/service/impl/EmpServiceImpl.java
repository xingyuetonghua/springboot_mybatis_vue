package com.aaa.springbootmybatisvue.service.impl;

import com.aaa.springbootmybatisvue.entity.Emp;
import com.aaa.springbootmybatisvue.dao.EmpMapper;
import com.aaa.springbootmybatisvue.service.IEmpService;
import com.aaa.springbootmybatisvue.util.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
@Service
public class EmpServiceImpl extends ServiceImpl<EmpMapper, Emp> implements IEmpService {

    @Resource
    private EmpMapper empMapper;

    @Override
    public Result<Page<Emp>> listByPage(Integer currentPage, Integer pageSize) {
        Page<Emp> page=new Page<>(currentPage,pageSize);
        Page<Emp> page1 = empMapper.selectPage(page, null);
        return new Result<>(200,"查询成功",page1);
    }
}
