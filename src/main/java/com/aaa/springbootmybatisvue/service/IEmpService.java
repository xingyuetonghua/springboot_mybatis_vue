package com.aaa.springbootmybatisvue.service;

import com.aaa.springbootmybatisvue.entity.Emp;
import com.aaa.springbootmybatisvue.util.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
public interface IEmpService extends IService<Emp> {
    public Result<Page<Emp>> listByPage(Integer currentPage, Integer pageSize);
}
