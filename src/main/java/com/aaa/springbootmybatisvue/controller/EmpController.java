package com.aaa.springbootmybatisvue.controller;


import com.aaa.springbootmybatisvue.entity.Emp;
import com.aaa.springbootmybatisvue.service.impl.EmpServiceImpl;
import com.aaa.springbootmybatisvue.util.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
@RestController
@RequestMapping("/emp")
@CrossOrigin
public class EmpController {

    @Autowired
    private EmpServiceImpl empService;

    @PostMapping("getAll/{currentPage}/{pageSize}")
    public Result<Page<Emp>> selectAll(@PathVariable Integer currentPage, @PathVariable Integer pageSize){

        return empService.listByPage(currentPage,pageSize);
    }

    @GetMapping("deletestu/{empno}")
    public Result deleteemp(@PathVariable Integer empno){
        boolean b = empService.removeById(empno);
        return Result.success(b);
    }

}

