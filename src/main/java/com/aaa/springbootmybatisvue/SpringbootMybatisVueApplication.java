package com.aaa.springbootmybatisvue;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
@MapperScan("com.aaa.springbootmybatisvue.dao")
public class SpringbootMybatisVueApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMybatisVueApplication.class, args);
    }


    //mybaties plus需要
    /*@Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }*/

    //mybaties plus需要
    /*@Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return configuration -> configuration.setUseDeprecatedExecutor(false);
    }*/


    //解决跨域问题
    /*@Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        //corsConfiguration.setAllowCredentials(true);
        corsConfiguration.addAllowedHeader("*");// 允许所有的头
        corsConfiguration.addAllowedOrigin("*");// 允许所有源发出的请求
        corsConfiguration.addAllowedMethod("*");// 允许所有的方法  GET POST
        source.registerCorsConfiguration("/**", corsConfiguration);// 所有的路径
        return new CorsFilter(source);
    }*/

}
