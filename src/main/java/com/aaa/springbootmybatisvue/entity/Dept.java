package com.aaa.springbootmybatisvue.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Dept对象", description="")
public class Dept implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键，部门编号")
    @TableId(value = "deptno", type = IdType.AUTO)
    private Integer deptno;

    @ApiModelProperty(value = "部门名称")
    private String dname;

    @ApiModelProperty(value = "地址")
    private String loc;


}
