package com.aaa.springbootmybatisvue.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaoxuan
 * @since 2023-03-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Emp对象", description="")
public class Emp implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "empno", type = IdType.AUTO)
    private Integer empno;

    private String ename;

    private String sex;

    private Double sal;

    private LocalDate hiredate;

    @ApiModelProperty(value = "部门的id")
    private Integer deptno;


}
