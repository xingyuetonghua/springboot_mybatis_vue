package com.aaa.springbootmybatisvue.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.function.Predicate;

/**
 * @program: qy160-demo
 * @description:
 * @author: 闫克起2
 * @create: 2023-03-07 15:23
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //封装了一个Docket类。完成swagger功能
    @Bean
    public Docket docket(){
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .groupName("qy160")
                .select()
                //只为com.aaa.controller包生成接口文档
                .apis(RequestHandlerSelectors.basePackage("com.aaa.springbootmybatisvue.controller"))
                .build();
        return docket;
    }
}
